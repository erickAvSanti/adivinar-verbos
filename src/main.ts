import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faUser,
	faBars,
	faEllipsisV,
	faQuestion,
	faPencilAlt,
	faTimes,
	faCheckCircle,
	faTimesCircle,
	faArrowRight,
	faTrophy,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faUser,
	faBars,
	faEllipsisV,
	faQuestion,
	faPencilAlt,
	faTimes,
	faCheckCircle,
	faTimesCircle,
	faArrowRight,
	faTrophy,
)

Vue.component('fai', FontAwesomeIcon)

import VueSweetalert2 from 'vue-sweetalert2';
 
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2);

Vue.use(require('@/plugins/index.js'))

import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
