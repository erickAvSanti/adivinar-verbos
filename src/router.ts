import Vue from 'vue'
import Router from 'vue-router'
import About from './views/About.vue'
import CompletePhrases from './views/CompletePhrases.vue'
import GuessWords from './views/GuessWords.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'About',
      component: About
    },
    {
      path: '/guess-words',
      name: 'GuessWords',
      component: GuessWords
    },
    {
      path: '/complete-phrases',
      name: 'CompletePhrases',
      component: CompletePhrases
    },
    { path: '*', redirect: { name: 'About' }}
  ]
})
