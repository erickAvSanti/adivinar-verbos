
/* eslint-disable */
function shuffleRandom(array) {
	array = array.split("")
	for (let i = array.length - 1; i > 0; i--) {
		const val = Math.random() * (i + 1)
		const j = Math.floor(val)
		let tmp = array[j]
		array[j] = array[i]
		array[i] = tmp
	}
	return array
}

function shuffle(array) {
  array.sort(() => Math.random() - 0.5)
  return array
}

export { shuffleRandom, shuffle }