
exports.install = function(Vue,options){
	Vue.prototype.$copy_selection = function(){
		document.execCommand('copy')
	}
	Vue.prototype.timezone = process.env.VUE_APP_TIMEZONE
	Vue.prototype.$time_to_load_list = 1200
	Vue.prototype.$dev_mode = process.env.NODE_ENV === 'development'

}
